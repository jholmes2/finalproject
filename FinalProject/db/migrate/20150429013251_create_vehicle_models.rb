class CreateVehicleModels < ActiveRecord::Migration
  def change
    create_table :vehicle_models do |t|
      t.string :model
      t.integer :vehicle_brand_id

      t.timestamps
    end
  end
end
