class CreateVehicleColors < ActiveRecord::Migration
  def change
    create_table :vehicle_colors do |t|
      t.string :color

      t.timestamps
    end
  end
end
