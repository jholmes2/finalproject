class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :customer_id
      t.string :integer
      t.integer :vehicle_id
      t.integer :employee_id
      t.string :price
      t.string :discount
      t.string :status
      t.string :interest
      t.string :months
      t.string :principal

      t.timestamps
    end
  end
end
