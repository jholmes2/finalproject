class CreateAmortRates < ActiveRecord::Migration
  def change
    create_table :amort_rates do |t|
      t.string :interest
      t.string :months
      t.string :principal

      t.timestamps
    end
  end
end
