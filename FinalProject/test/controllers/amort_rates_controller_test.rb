require 'test_helper'

class AmortRatesControllerTest < ActionController::TestCase
  setup do
    @amort_rate = amort_rates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amort_rates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amort_rate" do
    assert_difference('AmortRate.count') do
      post :create, amort_rate: { interest: @amort_rate.interest, months: @amort_rate.months, principal: @amort_rate.principal }
    end

    assert_redirected_to amort_rate_path(assigns(:amort_rate))
  end

  test "should show amort_rate" do
    get :show, id: @amort_rate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amort_rate
    assert_response :success
  end

  test "should update amort_rate" do
    patch :update, id: @amort_rate, amort_rate: { interest: @amort_rate.interest, months: @amort_rate.months, principal: @amort_rate.principal }
    assert_redirected_to amort_rate_path(assigns(:amort_rate))
  end

  test "should destroy amort_rate" do
    assert_difference('AmortRate.count', -1) do
      delete :destroy, id: @amort_rate
    end

    assert_redirected_to amort_rates_path
  end
end
