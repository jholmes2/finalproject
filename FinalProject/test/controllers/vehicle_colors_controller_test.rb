require 'test_helper'

class VehicleColorsControllerTest < ActionController::TestCase
  setup do
    @vehicle_color = vehicle_colors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vehicle_colors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vehicle_color" do
    assert_difference('VehicleColor.count') do
      post :create, vehicle_color: { color: @vehicle_color.color }
    end

    assert_redirected_to vehicle_color_path(assigns(:vehicle_color))
  end

  test "should show vehicle_color" do
    get :show, id: @vehicle_color
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vehicle_color
    assert_response :success
  end

  test "should update vehicle_color" do
    patch :update, id: @vehicle_color, vehicle_color: { color: @vehicle_color.color }
    assert_redirected_to vehicle_color_path(assigns(:vehicle_color))
  end

  test "should destroy vehicle_color" do
    assert_difference('VehicleColor.count', -1) do
      delete :destroy, id: @vehicle_color
    end

    assert_redirected_to vehicle_colors_path
  end
end
