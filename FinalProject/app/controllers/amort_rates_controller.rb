class AmortRatesController < ApplicationController
  before_action :set_amort_rate, only: [:show, :edit, :update, :destroy]

  # GET /amort_rates
  # GET /amort_rates.json
  def index
    @amort_rates = AmortRate.all
  end

  # GET /amort_rates/1
  # GET /amort_rates/1.json
  def show
  end

  # GET /amort_rates/new
  def new
    @amort_rate = AmortRate.new
  end

  # GET /amort_rates/1/edit
  def edit
  end

  # POST /amort_rates
  # POST /amort_rates.json
  def create
    @amort_rate = AmortRate.new(amort_rate_params)

    respond_to do |format|
      if @amort_rate.save
        format.html { redirect_to @amort_rate, notice: 'Amort rate was successfully created.' }
        format.json { render :show, status: :created, location: @amort_rate }
      else
        format.html { render :new }
        format.json { render json: @amort_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /amort_rates/1
  # PATCH/PUT /amort_rates/1.json
  def update
    respond_to do |format|
      if @amort_rate.update(amort_rate_params)
        format.html { redirect_to @amort_rate, notice: 'Amort rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @amort_rate }
      else
        format.html { render :edit }
        format.json { render json: @amort_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /amort_rates/1
  # DELETE /amort_rates/1.json
  def destroy
    @amort_rate.destroy
    respond_to do |format|
      format.html { redirect_to amort_rates_url, notice: 'Amort rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_amort_rate
      @amort_rate = AmortRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def amort_rate_params
      params.require(:amort_rate).permit(:interest, :months, :principal)
    end
end
