class VehicleColorsController < ApplicationController
  before_action :set_vehicle_color, only: [:show, :edit, :update, :destroy]

  # GET /vehicle_colors
  # GET /vehicle_colors.json
  def index
    @vehicle_colors = VehicleColor.all
  end

  # GET /vehicle_colors/1
  # GET /vehicle_colors/1.json
  def show
  end

  # GET /vehicle_colors/new
  def new
    @vehicle_color = VehicleColor.new
  end

  # GET /vehicle_colors/1/edit
  def edit
  end

  # POST /vehicle_colors
  # POST /vehicle_colors.json
  def create
    @vehicle_color = VehicleColor.new(vehicle_color_params)

    respond_to do |format|
      if @vehicle_color.save
        format.html { redirect_to @vehicle_color, notice: 'Vehicle color was successfully created.' }
        format.json { render :show, status: :created, location: @vehicle_color }
      else
        format.html { render :new }
        format.json { render json: @vehicle_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vehicle_colors/1
  # PATCH/PUT /vehicle_colors/1.json
  def update
    respond_to do |format|
      if @vehicle_color.update(vehicle_color_params)
        format.html { redirect_to @vehicle_color, notice: 'Vehicle color was successfully updated.' }
        format.json { render :show, status: :ok, location: @vehicle_color }
      else
        format.html { render :edit }
        format.json { render json: @vehicle_color.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vehicle_colors/1
  # DELETE /vehicle_colors/1.json
  def destroy
    @vehicle_color.destroy
    respond_to do |format|
      format.html { redirect_to vehicle_colors_url, notice: 'Vehicle color was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vehicle_color
      @vehicle_color = VehicleColor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vehicle_color_params
      params.require(:vehicle_color).permit(:color)
    end
end
