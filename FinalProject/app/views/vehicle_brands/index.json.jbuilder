json.array!(@vehicle_brands) do |vehicle_brand|
  json.extract! vehicle_brand, :id, :brand
  json.url vehicle_brand_url(vehicle_brand, format: :json)
end
