json.array!(@quotes) do |quote|
  json.extract! quote, :id, :customer_id, :integer, :vehicle_id, :employee_id, :price, :discount, :status, :interest, :months, :principal
  json.url quote_url(quote, format: :json)
end
