json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :year, :vehicle_color_id, :vehicle_type_id, :vehicle_model_id, :vehicle_brand_id, :vin, :price
  json.url vehicle_url(vehicle, format: :json)
end
