json.extract! @vehicle, :id, :year, :vehicle_color_id, :vehicle_type_id, :vehicle_model_id, :vehicle_brand_id, :vin, :price, :created_at, :updated_at
