json.array!(@amort_rates) do |amort_rate|
  json.extract! amort_rate, :id, :interest, :months, :principal
  json.url amort_rate_url(amort_rate, format: :json)
end
