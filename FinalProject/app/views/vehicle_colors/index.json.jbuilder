json.array!(@vehicle_colors) do |vehicle_color|
  json.extract! vehicle_color, :id, :color
  json.url vehicle_color_url(vehicle_color, format: :json)
end
