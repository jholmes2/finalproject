class Customer < ActiveRecord::Base

  belongs_to :quote
  has_one :status
  has_many :vehicles

end
