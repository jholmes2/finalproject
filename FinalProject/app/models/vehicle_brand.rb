class VehicleBrand < ActiveRecord::Base

  belongs_to :vehicle
  has_many :vehicle_models

end
