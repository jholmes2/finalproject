class Quote < ActiveRecord::Base

  has_many :employees
  has_many :customers
  has_many :vehicles

end
